// Homework195.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "ammmmm\n";
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Dog says Woof!!\n";
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Cat says Meow!!\n";
	}

};

class Cow : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Cow says Mooo!!\n";
	}

};

class Parrot : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Parrot says Caramba!!\n";
	}

};



int main()
{

	const int animalsNumber = 4;
	Animal* animals[animalsNumber];

	animals[0] = new Dog();
	animals[1] = new Cat();
	animals[2] = new Cow();
	animals[3] = new Parrot();
	
	for (int i = 0; i < animalsNumber; i++) {
		animals[i]->Voice();
	}
}

